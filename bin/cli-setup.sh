#!/usr/bin/env bash

# Based on https://github.com/danlynn/ember-cli-docker-template/blob/master/setup.sh

# Note that this will automatically be ran if you have rvm installed.
# This shell file sets up the following aliases whenever you cd into
# the current directory tree:
#
# + php
# + composer
# + PHP Cs Fixer
#
# If rvm is not installed then you can simply run:
#   . cli-setup.sh
#
# Note that these aliases revert back to executing the system version
# of each command whenever you exit the current project dir tree.

PREV_ROOT_DIR=$(readlink -f $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../..)
REGISTRY_MAIN_URL='registry.gitlab.com/seb2411/symfony-value-objects'
ENV_FILE="$PWD/.env"
VOLUME_PATH=$(pwd)

if [ -z "$DOCKER_TERM_OPTS" ]
then
  DOCKER_TERM_OPTS="-it --init"
fi

if [ -f "$ENV_FILE" ]
then
  export $(cat "$ENV_FILE" | xargs)
else
  printf '%s\n' "$ENV_FILE not found" >&2
fi

function refreshImages() {
  if [[ $PWD/ = $PREV_ROOT_DIR/* ]]; then
      eval "docker pull $REGISTRY_MAIN_URL/composer:latest"
      eval "docker pull $REGISTRY_MAIN_URL/php-cli:latest"
      eval "docker pull $REGISTRY_MAIN_URL/php-cs-fixer:latest"
  fi
}

function makeEnv() {
    envList=""

    for NAME in `compgen -e`
    do
        envList="${envList} -e ${NAME}=\"$(printenv ${NAME})\""
    done

    echo "$envList"
}

function php() {
  if [[ $PWD/ = $PREV_ROOT_DIR/* ]]; then
      args=( "$@" )
      eval "docker run ${DOCKER_TERM_OPTS} --rm \
          --user 1000:1000 \
          --ipc=host \
          --network=host \
          $(makeEnv) \
          -v $VOLUME_PATH:/var/www \
          -w /var/www \
          $REGISTRY_MAIN_URL/php-cli" "${args[@]}"
  else
      `which php` $@
  fi
}

function php-cs-fixer() {
  if [[ $PWD/ = $PREV_ROOT_DIR/* ]]; then
      args=( "$@" )
      eval "docker run ${DOCKER_TERM_OPTS} --rm \
          --user 1000:1000 \
          --ipc=host \
          --network=host \
          $(makeEnv) \
          -v $VOLUME_PATH:/var/www \
          -w /var/www \
          $REGISTRY_MAIN_URL/php-cs-fixer" "${args[@]}"
  else
      `which php` $@
  fi
}

function composer() {
  if [[ $PWD/ = $PREV_ROOT_DIR/* ]]; then
      args=( "$@" )
      eval "docker run ${DOCKER_TERM_OPTS} --rm \
          --user 1000:1000 \
          --ipc=host \
          --network=host \
          $(makeEnv) \
          -v $VOLUME_PATH:/var/www \
          -w /var/www \
          $REGISTRY_MAIN_URL/composer" "${args[@]}"
  else
      `which php` $@
  fi
}
