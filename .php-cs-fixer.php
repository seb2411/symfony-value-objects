<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__.'/src')
;

$config = new PhpCsFixer\Config();


$rules = [
    '@PSR2' => true,
    '@PSR12' => true,
    '@Symfony' => true,
    'cast_spaces' => [
        'space' => 'none',
    ],
    'concat_space' => [
        'spacing' => 'none',
    ],
    'native_function_invocation' => [
        'scope' => 'namespaced',
    ],
    'phpdoc_align' => [
        'align' => 'left',
    ],
    'array_syntax' => [
        'syntax' => 'short',
    ],
    'yoda_style' => true,
    'strict_param' => true,
];

return $config->setRules($rules)
    ->setFinder($finder)
;
